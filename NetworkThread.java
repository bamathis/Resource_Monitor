//Name: Brandon Mathis
//File Name: NetworkThread.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.util.*;
import javax.swing.*;

public class NetworkThread implements Runnable
	{
	static final String command = "netstat -tup";
	Vector<NetworkProcess> networkNames;
	boolean done;
	Process process;
	BufferedReader reader;
	Runtime runtime;
	Vector<NetworkProcess> outputArray;
	int count;
	JTable table;
	int arraySize;

//Constructor for the NetworkThread class
	NetworkThread(Runtime tempRuntime, JTable tempTable)
		{
		runtime = tempRuntime;
		table = tempTable;
		networkNames = new Vector();
		outputArray = new Vector();
		done = false;
		new Thread(this).start();
		}
//=========================================
//Stops the thread from looping again
	void stopThread()
		{
		done = true;
		}
//=====================================================
//Creates a process and calls calculateTopResourceHogs()
	void runCommand()throws Exception
		{
		if(process != null)					//If there is already a process
			process.destroy();
		process = runtime.exec(command);
		reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		calculateTopResourcesHogs();
		}
//===============================================
//Reads from the process and calls calculate()
	void calculateTopResourcesHogs() throws Exception
		{
		boolean found;
		String[] temp;
		int id;
		NetworkProcess tempProcess;
		String name;
		try
			{
			reader.readLine();
			reader.readLine();
			reader.readLine();
			reader.readLine();
			while(true)
				{
				System.out.println("1");
				found = false;
				temp = reader.readLine().split("\\s+");
				System.out.println(temp[6]);
				if(temp[6].contains("/") && !temp[6].trim().equals(""))
					{
					name = temp[6].substring(temp[6].indexOf('/')).trim();
					id = Integer.parseInt(temp[6].substring(0,temp[6].indexOf("/")).trim());
					for(int n = 0; n < outputArray.size() && !found; n++)
						{
						found = outputArray.elementAt(n).id == id;
						if(found)
							outputArray.elementAt(n).network++;
						}
					if(!found)
						{
						tempProcess = new NetworkProcess(name,id);
						outputArray.add(tempProcess);
						}
					}
				}
			}
		catch(Exception e)
			{
			arraySize = outputArray.size();
			if(arraySize > 5)
				arraySize = 5;
			compare();
			}
		outputArray.removeAllElements();
		}
//==========================================
//Finds the most resource heavy processes
	void compare()
		{
		int size;
		networkNames.removeAllElements();
		count = 0;
		size = outputArray.size();
		for(int n = 0; n < size; n++)
			{
			if(count >= 5)
				{
				sort();
				if(outputArray.elementAt(n).networkIsLarger(networkNames.elementAt(4).network))
					{
					networkNames.removeElementAt(4);
					networkNames.add(outputArray.elementAt(n));
					}
				}
			else
				{
				networkNames.add(outputArray.elementAt(n));
				count++;
				}
			}
		sort();
    	}
//==========================================
//Sorts the top 5 process list into descending order
    void sort()
    	{
    	NetworkProcess tempNetwork[];
    	NetworkProcess temp;
    	tempNetwork = new NetworkProcess[5];
    	for(int n = 0; n < arraySize; n++)
    		tempNetwork[n] = networkNames.elementAt(n);
    	networkNames.removeAllElements();
    	for(int n = 0; n < arraySize; n++)
    		for(int m = n + 1; m < arraySize; m++)
    			{
    			if(!tempNetwork[n].networkIsLarger(tempNetwork[m].network))
    				{
    				temp = tempNetwork[n];
    				tempNetwork[n] = tempNetwork[m];
    				tempNetwork[m] = temp;
    				}
    			}
    	for(int n = 0; n < arraySize; n++)
    		networkNames.add(tempNetwork[n]);
    	}
//==========================================
//Updates JList while thread isn't stopped
	public void run()
		{

		try
			{
			while(!done)
				{
				runCommand();
				SwingUtilities.invokeLater(new NetworkLabelUpdater(networkNames,table,arraySize));
				Thread.sleep(5000);
				}
			}
		catch(Exception e)
			{
			e.printStackTrace();
			System.out.println("Caught error in run()");
			}
		}
	}