//Name: Brandon Mathis
//File Name: DiskThread.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.util.*;
import javax.swing.*;

public class DiskThread implements Runnable
	{
	static final String command = "pidstat -d";
	Vector<DiskProcess> inputNames;
	Vector<DiskProcess> outputNames;
	boolean done;
	Process process;
	BufferedReader reader;
	Runtime runtime;
	Vector<DiskProcess> outputArray;
	int count;
	JTable table;
	int arraySize;

//Constructor for the DiskThread class
	DiskThread(Runtime tempRuntime, JTable tempTable)
		{
		runtime = tempRuntime;
		table = tempTable;
		inputNames = new Vector();
		outputNames = new Vector();
		outputArray = new Vector();
		done = false;
		new Thread(this).start();
		}
//=========================================
//Stops the thread from looping again
	void stopThread()
		{
		done = true;
		}
//=====================================================
//Creates a process and calls calculateTopResourceHogs()
	void runCommand()throws Exception
		{
		if(process != null)					//If there is already a process
			process.destroy();
		process = runtime.exec(command);
		reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		calculateTopResourcesHogs();
		}
//===============================================
//Reads from the process and calls calculate()
	void calculateTopResourcesHogs() throws Exception
		{
		String[] temp;
		DiskProcess tempProcess;
		String output;
		String error;
		output = "";
		try
			{
			reader.readLine();
			reader.readLine();
			reader.readLine();
			while(true)
				{
				temp = reader.readLine().split("\\s+");
				tempProcess = new DiskProcess(temp[7].trim(),temp[3].trim(),temp[4].trim(),temp[5].trim());
				outputArray.add(tempProcess);
				}
			}
		catch(Exception e)
			{
			arraySize = outputArray.size();
			if(arraySize > 5)
				arraySize = 5;
			compare();
			}
		outputArray.removeAllElements();
		}
//==========================================
//Finds the most resource heavy processes
	void compare()
		{
		int size;
		inputNames.removeAllElements();
		outputNames.removeAllElements();
		count = 0;
		size = outputArray.size();
		for(int n = 0; n < size; n++)
			{
			if(count >= 5)
				{
				sort();
				if(outputArray.elementAt(n).inputIsLarger(inputNames.elementAt(4).input))
					{
					inputNames.removeElementAt(4);
					inputNames.add(outputArray.elementAt(n));
					}
				if(outputArray.elementAt(n).outputIsLarger(outputNames.elementAt(4).output))
					{
					outputNames.removeElementAt(4);
					outputNames.add(outputArray.elementAt(n));
					}
				}
			else
				{
				inputNames.add(outputArray.elementAt(n));
				outputNames.add(outputArray.elementAt(n));
				count++;
				}
			}
		sort();
    	}
//==========================================
//Sorts the top 5 process list into descending order
   void sort()
    	{
    	DiskProcess tempInput[];
    	DiskProcess tempOutput[];
    	DiskProcess temp;
    	tempInput = new DiskProcess[5];
    	tempOutput = new DiskProcess[5];
    	for(int n = 0; n < arraySize; n++)
    		{
    		tempInput[n] = inputNames.elementAt(n);
    		tempOutput[n] = outputNames.elementAt(n);
    		}
    	inputNames.removeAllElements();
    	outputNames.removeAllElements();
    	for(int n = 0; n < arraySize; n++)
    		for(int m = n + 1; m < arraySize; m++)
    			{
    			if(!tempInput[n].inputIsLarger(tempInput[m].input))
    				{
    				temp = tempInput[n];
    				tempInput[n] = tempInput[m];
    				tempInput[m] = temp;
    				}
    			if(!tempOutput[n].outputIsLarger(tempOutput[m].output))
    				{
    				temp = tempOutput[n];
    				tempOutput[n] = tempOutput[m];
    				tempOutput[m] = temp;
    				}
    			}
    	for(int n = 0; n < arraySize; n++)
    		{
    		inputNames.add(tempInput[n]);
    		outputNames.add(tempOutput[n]);
    		}
    	}
//==========================================
//Updates JList while thread isn't stopped
	public void run()
		{

		try
			{
			while(!done)
				{
				runCommand();
				SwingUtilities.invokeLater(new DiskLabelUpdater(inputNames,outputNames,table,arraySize));
				Thread.sleep(5000);
				}
			}
		catch(Exception e)
			{
				e.printStackTrace();
			System.out.println("Caught error in run()");
			}
		}
	}