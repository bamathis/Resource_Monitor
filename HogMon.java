//Name: Brandon Mathis
//File Name: HogMon.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

public class HogMon
	{
	public static void main(String[] args)
		{
		new ProcessWindow();
		}
	}
//########################################################
class ProcessWindow extends JFrame
	{
	DefaultTableModel model;
	Runtime runtime;
	CPUThread cpuThread;
	DiskThread diskThread;
	NetworkThread networkThread;
	JTable table;
	static final String[] cpuHeaders = {"CPU Usage", "PID", "Name","User","Percentage"};
	static final String[] memHeaders = {"Memory Usage", "PID", "Name","User","Percentage"};
	static final String[] inputHeaders = {"Disc Input", "PID", "Name","kB read/s"};
	static final String[] outputHeaders = {"Disc Output", "PID", "Name","kB written/s"};
	static final String[] networkHeaders = {"Network", "PID", "Name","Connections"};

//Constructor for the ProcessWindow class
	ProcessWindow()
		{

		runtime = Runtime.getRuntime();
		model = new DefaultTableModel(34,5);
		table = new JTable(30,5)
			{
       		public boolean isCellEditable(int row,int column)
       			{
        		return false;
       			}
     		};
		table.setModel(model);
		table.setGridColor(Color.black);
		table.setMinimumSize(new Dimension(10, 10));
		for(int n = 0; n < 5; n++)
			{
			table.setValueAt(cpuHeaders[n],0,n);
			table.setValueAt(memHeaders[n],7,n);
			if(n < 4)
				{
				table.setValueAt(inputHeaders[n],14,n);
				table.setValueAt(outputHeaders[n],21,n);
				table.setValueAt(networkHeaders[n],28,n);
				}
			}
		for(int n = 1; n < 35; n = n + 7)
			{
			for(int m = 1; m < 6; m++)
				table.setValueAt(m, m + n - 1,0);
			}
		cpuThread = new CPUThread(runtime,table);
		diskThread = new DiskThread(runtime,table);
		networkThread = new NetworkThread(runtime,table);
		getContentPane().add(table);
		setupMainFrame();
		}

//========================================
//Sets up the JFrame
	void setupMainFrame()
		{
        Toolkit  tk;
        Dimension d;
        tk = Toolkit.getDefaultToolkit();
        d = tk.getScreenSize();
        setSize(d.width/2, d.height/2);
        setMinimumSize(new Dimension(d.width/3 , d.height/3));
        setLocation(d.width/4,d.height/4);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("HogMon");
        setVisible(true);
		}
	}
