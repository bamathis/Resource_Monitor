import java.io.*;
import java.util.*;
import javax.swing.*;

public class CPUThread implements Runnable
	{
	static final String command = "ps u -A";
	Vector<RunningProcess> cpuNames;
	Vector<RunningProcess> memNames;
	boolean done;
	Process process;
	BufferedReader reader;
	Runtime runtime;
	Vector<RunningProcess> outputArray;
	int count;
	JTable table;
	int arraySize;

	CPUThread(Runtime tempRuntime, JTable tempTable)
		{
		runtime = tempRuntime;
		table = tempTable;
		cpuNames = new Vector();
		memNames = new Vector();
		outputArray = new Vector();
		done = false;
		new Thread(this).start();
		}
//=========================================
	void stopThread()
		{
		done = true;
		}
//=====================================================
//Creates a process and calls calculateTopResourceHogs()
	void runCommand()throws Exception
		{
		if(process != null)					//If there is already a process
			process.destroy();
		process = runtime.exec(command);
		reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		calculateTopResourcesHogs();
		}
//===============================================
//Reads from the process and calls calculate()
	void calculateTopResourcesHogs() throws Exception
		{
		String[] temp;
		RunningProcess tempProcess;
		String output;
		String error;
		output = "";
		try
			{
			reader.readLine();
			while(true)
				{
				temp = reader.readLine().split("\\s+");
				tempProcess = new RunningProcess(temp[10].trim(),temp[1].trim(),temp[2].trim(),temp[3].trim(),temp[0].trim());
				outputArray.add(tempProcess);
				}
			}
		catch(Exception e)
			{
			arraySize = outputArray.size();
			if(arraySize > 5)
				arraySize = 5;
			}
		compare();
		outputArray.removeAllElements();
		}
//==========================================
//Finds the most resource heavy processes
	void compare()
		{
		int size;
		cpuNames.removeAllElements();
		memNames.removeAllElements();
		count = 0;
		size = outputArray.size();
		for(int n = 0; n < size; n++)
			{
			if(count >= 5)
				{
				sort();
				if(outputArray.elementAt(n).cpuIsLarger(cpuNames.elementAt(4).cpu))
					{
					cpuNames.removeElementAt(4);
					cpuNames.add(outputArray.elementAt(n));
					}
				if(outputArray.elementAt(n).memoryIsLarger(memNames.elementAt(4).memory))
					{
					memNames.removeElementAt(4);
					memNames.add(outputArray.elementAt(n));
					}
				}
			else
				{
				cpuNames.add(outputArray.elementAt(n));
				memNames.add(outputArray.elementAt(n));
				count++;
				}
			}
		sort();
    	}
//==========================================
//Sorts the top 5 process list into descending order
  	void sort()
    	{
    	RunningProcess tempCpu[];
    	RunningProcess tempMem[];
    	RunningProcess temp;
    	tempCpu = new RunningProcess[5];
    	tempMem = new RunningProcess[5];
    	for(int n = 0; n < 5; n++)
    		{
    		tempCpu[n] = cpuNames.elementAt(n);
    		tempMem[n] = memNames.elementAt(n);
    		}
    	cpuNames.removeAllElements();
    	memNames.removeAllElements();
    	for(int n = 0; n < arraySize; n++)
    		for(int m = n + 1; m < arraySize; m++)
    			{
    			if(!tempCpu[n].cpuIsLarger(tempCpu[m].cpu))
    				{
    				temp = tempCpu[n];
    				tempCpu[n] = tempCpu[m];
    				tempCpu[m] = temp;
    				}
    			if(!tempMem[n].memoryIsLarger(tempMem[m].memory))
    				{
    				temp = tempMem[n];
    				tempMem[n] = tempMem[m];
    				tempMem[m] = temp;
    				}
    			}
    	for(int n = 0; n < arraySize; n++)
    		{
    		cpuNames.add(tempCpu[n]);
    		memNames.add(tempMem[n]);
    		}
    	}
//==========================================
//Updates JList while thread isn't stopped
	public void run()
		{

		try
			{
			while(!done)
				{
				runCommand();
				SwingUtilities.invokeLater(new CPULabelUpdater(cpuNames,memNames,table,arraySize));
				Thread.sleep(5000);
				}
			}
		catch(Exception e)
			{
				e.printStackTrace();
			System.out.println("Caught error in run()");
			}
		}
	}