//Name: Brandon Mathis
//File Name: NetworkProcess.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

public class NetworkProcess
	{
	int id;
	int network;
	String process;

//Constructor for NetworkProcess class
	NetworkProcess(String tempProcess,int tempId)
		{
		process = tempProcess;
		id = tempId;
		network = 1;
		}
//=================================================
//Returns whether the current process has more connections than passed one
	boolean networkIsLarger(int otherNetwork)
		{
		return network > otherNetwork;
		}
	}