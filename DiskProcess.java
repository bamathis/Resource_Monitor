//Name: Brandon Mathis
//File Name: DiskProcess.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

public class DiskProcess
	{
	int id;
	double input;
	double output;
	String process;

//Constructor for the DiskProcess class
	DiskProcess(String tempProcess,String tempId, String tempInput, String tempOutput)
		{
		process = tempProcess;
		id = Integer.parseInt(tempId);
		output = Double.parseDouble(tempOutput);
		input = Double.parseDouble(tempInput);
		}
//=================================================
//Returns whether the current input/read is bigger than the passed
	boolean inputIsLarger(double otherInput)
		{
		return input > otherInput;
		}
//=================================================
//Returns whether the current output/write is bigger than the passed
	boolean outputIsLarger(double otherOutput)
		{
		return output > otherOutput;
		}
	}