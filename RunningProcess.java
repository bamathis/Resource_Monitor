//Name: Brandon Mathis
//File Name: RunningProcess.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

public class RunningProcess
	{
	int id;
	double memory;
	double cpu;
	String process;
	String user;

//Constructor for the RunningProcess class
	RunningProcess(String tempProcess,String tempId, String tempCpu, String tempMemory,String tempUser)
		{
		user = tempUser;
		process = tempProcess;
		id = Integer.parseInt(tempId);
		memory = Double.parseDouble(tempMemory);
		cpu = Double.parseDouble(tempCpu);
		}
//=================================================
//Returns whether the memory usage for the current instance is larger than the passed one
	boolean memoryIsLarger(double otherMemory)
		{
		return memory > otherMemory;
		}
//=================================================
//Returns whether the cpu usage for the current instance is larger than the passed one
	boolean cpuIsLarger(double otherCpu)
		{
		return cpu > otherCpu;
		}
	}