//Name: Brandon Mathis
//File Name: CPULabelUpdater.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.util.*;
import javax.swing.*;

public class CPULabelUpdater implements Runnable
	{
	Vector<RunningProcess> cpuNames;
	Vector<RunningProcess> memNames;
	JTable table;
	int size;

//Constructor for the CPULabelUpdater class
	CPULabelUpdater(Vector<RunningProcess> tempCpuNames, Vector<RunningProcess> tempMemNames, JTable tempTable,int tempSize)
		{
		size = tempSize;
		cpuNames = tempCpuNames;
		memNames = tempMemNames;
		table = tempTable;
		}
//=================================
//Updates the values for CPU and Memory rows of the JList
	public void run()
		{
		for(int n = 1; n < size + 1; n++)
			{
			table.setValueAt("" + cpuNames.elementAt(n-1).id,n,1);
			table.setValueAt("" + cpuNames.elementAt(n-1).process,n,2);
			table.setValueAt("" + cpuNames.elementAt(n-1).user,n,3);
			table.setValueAt("" + cpuNames.elementAt(n-1).cpu,n,4);

			table.setValueAt("" + memNames.elementAt(n-1).id,n + 7,1);
			table.setValueAt("" + memNames.elementAt(n-1).process,n + 7,2);
			table.setValueAt("" + memNames.elementAt(n-1).user,n + 7,3);
			table.setValueAt("" + memNames.elementAt(n-1).memory,n + 7,4);

			}
		}
	}