//Name: Brandon Mathis
//File Name: NetworkLabelUpdater.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.util.*;
import javax.swing.*;

public class NetworkLabelUpdater implements Runnable
	{
	Vector<NetworkProcess> networkNames;
	Vector<NetworkProcess> outputNames;
	JTable table;
	int size;

//Constructor for the NetworkLabelUpdater class
	NetworkLabelUpdater(Vector<NetworkProcess> tempNetworkNames, JTable tempTable,int tempSize)
		{
		size = tempSize;
		networkNames = tempNetworkNames;
		table = tempTable;
		}
//=================================
//Updates the rows for the network in the JList
	public void run()
		{
		for(int n = 1; n < size + 1; n++)
			{
			table.setValueAt("" + networkNames.elementAt(n-1).id,28 + n,1);
			table.setValueAt("" + networkNames.elementAt(n-1).process,28 + n,2);
			table.setValueAt("" + networkNames.elementAt(n-1).network,28 + n,3);
			}
		}
	}