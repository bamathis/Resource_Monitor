//Name: Brandon Mathis
//File Name: DiskLabelUpdater.java
//7 October, 2015
//Program Description: Displays 5 most resource heavy processes for cpu,memory, disk I/O, and network

import java.io.*;
import java.util.*;
import javax.swing.*;

public class DiskLabelUpdater implements Runnable
	{
	Vector<DiskProcess> inputNames;
	Vector<DiskProcess> outputNames;
	JTable table;
	int size;

//Constructor for the DiskLabelUpdater class
	DiskLabelUpdater(Vector<DiskProcess> tempInputNames, Vector<DiskProcess> tempOutputNames, JTable tempTable,int tempSize)
		{
		size = tempSize;
		inputNames = tempInputNames;
		outputNames = tempOutputNames;
		table = tempTable;
		}
//=================================
//Updates the Disk I/O rows in the JList
	public void run()
		{
		for(int n = 1; n < size + 1; n++)
			{
			table.setValueAt("" + inputNames.elementAt(n-1).id,14 + n,1);
			table.setValueAt("" + inputNames.elementAt(n-1).process,14 + n,2);
			table.setValueAt("" + inputNames.elementAt(n-1).input,14 + n,3);

			table.setValueAt("" + outputNames.elementAt(n-1).id,n + 21,1);
			table.setValueAt("" + outputNames.elementAt(n-1).process,n + 21,2);
			table.setValueAt("" + outputNames.elementAt(n-1).output,n + 21,3);

			}
		}
	}