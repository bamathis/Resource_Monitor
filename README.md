# Resource_Monitor

[Resource_Monitor](https://gitlab.com/bamathis/Resource_Monitor) is a Linux resource monitor that displays the 5 most resource heavy processes for cpu,memory, disk I/O, and network.

## Quick Start

### Dependencies

- Java 7+

### Program Execution

```
$ java HogMon
```

